/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 29 Sep, 2019 5:56:19 PM                     ---
 * ----------------------------------------------------------------
 */
package com.deloitte.telegram.integration.jalo;

import com.deloitte.telegram.integration.constants.TelegramintegrationConstants;
import com.deloitte.telegram.integration.jalo.TelegramAbandonedCartNotificationCronJob;
import de.hybris.platform.directpersistence.annotation.SLDSafe;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.jalo.user.User;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>TelegramintegrationManager</code>.
 */
@SuppressWarnings({"unused","cast","PMD"})
@SLDSafe
public class TelegramintegrationManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("telegramNotification", AttributeMode.INITIAL);
		tmp.put("telegramChatId", AttributeMode.INITIAL);
		tmp.put("telegramUniqueRequestId", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.Customer", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("telegramNotificationSent", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.AbstractOrder", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	public TelegramAbandonedCartNotificationCronJob createTelegramAbandonedCartNotificationCronJob(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType("TelegramAbandonedCartNotificationCronJob");
			return (TelegramAbandonedCartNotificationCronJob)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating TelegramAbandonedCartNotificationCronJob : "+e.getMessage(), 0 );
		}
	}
	
	public TelegramAbandonedCartNotificationCronJob createTelegramAbandonedCartNotificationCronJob(final Map attributeValues)
	{
		return createTelegramAbandonedCartNotificationCronJob( getSession().getSessionContext(), attributeValues );
	}
	
	public static final TelegramintegrationManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (TelegramintegrationManager) em.getExtension(TelegramintegrationConstants.EXTENSIONNAME);
	}
	
	@Override
	public String getName()
	{
		return TelegramintegrationConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramChatId</code> attribute.
	 * @return the telegramChatId - Will be the telegram chat ID number
	 */
	public String getTelegramChatId(final SessionContext ctx, final Customer item)
	{
		return (String)item.getProperty( ctx, TelegramintegrationConstants.Attributes.Customer.TELEGRAMCHATID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramChatId</code> attribute.
	 * @return the telegramChatId - Will be the telegram chat ID number
	 */
	public String getTelegramChatId(final Customer item)
	{
		return getTelegramChatId( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramChatId</code> attribute. 
	 * @param value the telegramChatId - Will be the telegram chat ID number
	 */
	public void setTelegramChatId(final SessionContext ctx, final Customer item, final String value)
	{
		item.setProperty(ctx, TelegramintegrationConstants.Attributes.Customer.TELEGRAMCHATID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramChatId</code> attribute. 
	 * @param value the telegramChatId - Will be the telegram chat ID number
	 */
	public void setTelegramChatId(final Customer item, final String value)
	{
		setTelegramChatId( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramNotification</code> attribute.
	 * @return the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public Boolean isTelegramNotification(final SessionContext ctx, final Customer item)
	{
		return (Boolean)item.getProperty( ctx, TelegramintegrationConstants.Attributes.Customer.TELEGRAMNOTIFICATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramNotification</code> attribute.
	 * @return the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public Boolean isTelegramNotification(final Customer item)
	{
		return isTelegramNotification( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramNotification</code> attribute. 
	 * @return the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public boolean isTelegramNotificationAsPrimitive(final SessionContext ctx, final Customer item)
	{
		Boolean value = isTelegramNotification( ctx,item );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramNotification</code> attribute. 
	 * @return the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public boolean isTelegramNotificationAsPrimitive(final Customer item)
	{
		return isTelegramNotificationAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramNotification</code> attribute. 
	 * @param value the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public void setTelegramNotification(final SessionContext ctx, final Customer item, final Boolean value)
	{
		item.setProperty(ctx, TelegramintegrationConstants.Attributes.Customer.TELEGRAMNOTIFICATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramNotification</code> attribute. 
	 * @param value the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public void setTelegramNotification(final Customer item, final Boolean value)
	{
		setTelegramNotification( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramNotification</code> attribute. 
	 * @param value the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public void setTelegramNotification(final SessionContext ctx, final Customer item, final boolean value)
	{
		setTelegramNotification( ctx, item, Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramNotification</code> attribute. 
	 * @param value the telegramNotification - Decides whether a customer can notifications in Telegram
	 */
	public void setTelegramNotification(final Customer item, final boolean value)
	{
		setTelegramNotification( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.telegramNotificationSent</code> attribute.
	 * @return the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public Boolean isTelegramNotificationSent(final SessionContext ctx, final AbstractOrder item)
	{
		return (Boolean)item.getProperty( ctx, TelegramintegrationConstants.Attributes.AbstractOrder.TELEGRAMNOTIFICATIONSENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.telegramNotificationSent</code> attribute.
	 * @return the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public Boolean isTelegramNotificationSent(final AbstractOrder item)
	{
		return isTelegramNotificationSent( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.telegramNotificationSent</code> attribute. 
	 * @return the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public boolean isTelegramNotificationSentAsPrimitive(final SessionContext ctx, final AbstractOrder item)
	{
		Boolean value = isTelegramNotificationSent( ctx,item );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.telegramNotificationSent</code> attribute. 
	 * @return the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public boolean isTelegramNotificationSentAsPrimitive(final AbstractOrder item)
	{
		return isTelegramNotificationSentAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.telegramNotificationSent</code> attribute. 
	 * @param value the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public void setTelegramNotificationSent(final SessionContext ctx, final AbstractOrder item, final Boolean value)
	{
		item.setProperty(ctx, TelegramintegrationConstants.Attributes.AbstractOrder.TELEGRAMNOTIFICATIONSENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.telegramNotificationSent</code> attribute. 
	 * @param value the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public void setTelegramNotificationSent(final AbstractOrder item, final Boolean value)
	{
		setTelegramNotificationSent( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.telegramNotificationSent</code> attribute. 
	 * @param value the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public void setTelegramNotificationSent(final SessionContext ctx, final AbstractOrder item, final boolean value)
	{
		setTelegramNotificationSent( ctx, item, Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.telegramNotificationSent</code> attribute. 
	 * @param value the telegramNotificationSent - Stores whether the Telegram notification was sent or not for an order
	 */
	public void setTelegramNotificationSent(final AbstractOrder item, final boolean value)
	{
		setTelegramNotificationSent( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramUniqueRequestId</code> attribute.
	 * @return the telegramUniqueRequestId - Will be the telegram unique request ID number
	 */
	public String getTelegramUniqueRequestId(final SessionContext ctx, final Customer item)
	{
		return (String)item.getProperty( ctx, TelegramintegrationConstants.Attributes.Customer.TELEGRAMUNIQUEREQUESTID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.telegramUniqueRequestId</code> attribute.
	 * @return the telegramUniqueRequestId - Will be the telegram unique request ID number
	 */
	public String getTelegramUniqueRequestId(final Customer item)
	{
		return getTelegramUniqueRequestId( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramUniqueRequestId</code> attribute. 
	 * @param value the telegramUniqueRequestId - Will be the telegram unique request ID number
	 */
	public void setTelegramUniqueRequestId(final SessionContext ctx, final Customer item, final String value)
	{
		item.setProperty(ctx, TelegramintegrationConstants.Attributes.Customer.TELEGRAMUNIQUEREQUESTID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.telegramUniqueRequestId</code> attribute. 
	 * @param value the telegramUniqueRequestId - Will be the telegram unique request ID number
	 */
	public void setTelegramUniqueRequestId(final Customer item, final String value)
	{
		setTelegramUniqueRequestId( getSession().getSessionContext(), item, value );
	}
	
}
