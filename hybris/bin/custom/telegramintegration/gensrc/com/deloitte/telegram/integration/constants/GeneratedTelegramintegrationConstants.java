/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 29 Sep, 2019 5:56:19 PM                     ---
 * ----------------------------------------------------------------
 */
package com.deloitte.telegram.integration.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTelegramintegrationConstants
{
	public static final String EXTENSIONNAME = "telegramintegration";
	public static class TC
	{
		public static final String TELEGRAMABANDONEDCARTNOTIFICATIONCRONJOB = "TelegramAbandonedCartNotificationCronJob".intern();
	}
	public static class Attributes
	{
		public static class AbstractOrder
		{
			public static final String TELEGRAMNOTIFICATIONSENT = "telegramNotificationSent".intern();
		}
		public static class Customer
		{
			public static final String TELEGRAMCHATID = "telegramChatId".intern();
			public static final String TELEGRAMNOTIFICATION = "telegramNotification".intern();
			public static final String TELEGRAMUNIQUEREQUESTID = "telegramUniqueRequestId".intern();
		}
	}
	
	protected GeneratedTelegramintegrationConstants()
	{
		// private constructor
	}
	
	
}
