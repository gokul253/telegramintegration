/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.customer.facade;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

/**
 *author gs
 *Kindly reuse your custom facade extension register customer method. Meant for POC purpose only
 */
public class DefaultTelegramIntegrationCustomerFacade extends DefaultCustomerFacade
{
    @Resource(name = "telegramUniqueCodeGenerator")
    private KeyGenerator telegramUniqueCodeGenerator;

    @Override
    public void register(final RegisterData registerData) throws DuplicateUidException
    {
        validateParameterNotNullStandardMessage("registerData", registerData);
        Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
        Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
        Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

        final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
        newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));

        if (StringUtils.isNotBlank(registerData.getFirstName()) && StringUtils.isNotBlank(registerData.getLastName()))
        {
            newCustomer.setName(
                    getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
        }
        final TitleModel title = getUserService().getTitleForCode(registerData.getTitleCode());
        newCustomer.setTitle(title);
        setUidForRegister(registerData, newCustomer);
        newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
        newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
        newCustomer.setTelegramNotification(registerData.getTelegramNotification());
        newCustomer.setTelegramUniqueRequestId(telegramUniqueCodeGenerator.generate().toString());
        getCustomerAccountService().register(newCustomer, registerData.getPassword());
    }
}
