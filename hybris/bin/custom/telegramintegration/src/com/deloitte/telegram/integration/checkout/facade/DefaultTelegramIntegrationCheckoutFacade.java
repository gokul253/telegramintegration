/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.checkout.facade;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.deloitte.telegram.integration.service.TelegramintegrationService;

/**
 *author gs
 *Kindly reuse your custom facade extension place order method. Meant for POC purpose only
 */
public class DefaultTelegramIntegrationCheckoutFacade extends DefaultAcceleratorCheckoutFacade
{
    @Resource(name = "telegramintegrationService")
    private TelegramintegrationService telegramIntegrationService;

    @Override
    protected OrderModel placeOrder(final CartModel cartModel) throws InvalidCartException
    {
        final UiExperienceLevel uiExperienceLevel = getUiExperienceService().getUiExperienceLevel();
        final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
        parameter.setEnableHooks(true);
        parameter.setCart(cartModel);
        if (UiExperienceLevel.MOBILE.equals(uiExperienceLevel))
        {
            // Set application to WebMobile
            parameter.setSalesApplication(SalesApplication.WEBMOBILE);
            return getCommerceCheckoutService().placeOrder(parameter).getOrder();
        }
        // Default to WEB
        parameter.setSalesApplication(SalesApplication.WEB);
        final OrderModel order = getCommerceCheckoutService().placeOrder(parameter).getOrder();
        telegramIntegrationService.sendOrderDetailsDocument(order);
        return order;
    }

    @Override
    public OrderData placeOrder() throws InvalidCartException
    {
        final CartModel cartModel = getCart();
        if (cartModel != null)
        {
            if (cartModel.getUser().equals(getCurrentUserForCheckout())
                    || getCheckoutCustomerStrategy().isAnonymousCheckout())
            {
                beforePlaceOrder(cartModel);
                final OrderModel orderModel = placeOrder(cartModel);
                afterPlaceOrder(cartModel, orderModel);
                if (orderModel != null)
                {
                    final OrderData orderDetails = getOrderConverter().convert(orderModel);
                    final List<CouponData> giftCoupons = orderDetails.getAppliedOrderPromotions().stream()
                            .filter(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes()))
                            .flatMap(p -> p.getGiveAwayCouponCodes().stream()).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(giftCoupons))
                    {
                        telegramIntegrationService.sendGiveAwayCouponMessage(giftCoupons, orderModel);
                    }
                    return orderDetails;

                }
            }
        }
        return null;
    }

}
