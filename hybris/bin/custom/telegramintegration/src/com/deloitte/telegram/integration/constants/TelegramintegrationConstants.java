/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.deloitte.telegram.integration.constants;

/**
 * Global class for all Telegramintegration constants. You can add global constants for your extension into this class.
 */
public final class TelegramintegrationConstants extends GeneratedTelegramintegrationConstants
{
    public static final String EXTENSIONNAME = "telegramintegration";

    private TelegramintegrationConstants()
    {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "telegramintegrationPlatformLogo";

    public static final String TELEGRAM_BOT_ID = "telegram.integration.bot.id";

    public static final String TELEGRAM_AUTH_TOKEN = "telegram.integration.bot.auth.token";

    public static final String TELEGRAM_GET_UPDATES = "/getUpdates";

    public static final String TELEGRAM_SEND_MESSAGE = "/sendMessage";

    public static final String TELEGRAM_START = "?start=";

    public static final String TELEGRAM_OPT_IN_URL = "telegram.integration.opt.in.url";

    public static final String HTTPS_POST = "POST";

    public static final String TELEGRAM_SEND_DOCUMENT = "/sendDocument";

}
