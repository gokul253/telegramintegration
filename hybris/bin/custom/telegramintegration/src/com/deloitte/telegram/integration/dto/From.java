package com.deloitte.telegram.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *author gs
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "is_bot", "first_name", "last_name", "username", "language_code" })
public class From
{
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("is_bot")
    private boolean is_bot;

    @JsonProperty("first_name")
    private String first_name;

    @JsonProperty("last_name")
    private String last_name;

    @JsonProperty("username")
    private String username;

    @JsonProperty("language_code")
    private String language_code;

    /**
     * No args constructor for use in serialization
     *
     */
    public From()
    {}

    /**
    *
    * @param id
    * @param is_bot
    * @param first_name
    * @param last_name
    * @param username
    * @param language_code
    */
    public From(final Integer id, final boolean is_bot, final String first_name, final String last_name,
            final String username, final String language_code)
    {
        super();
        this.id = id;
        this.is_bot = is_bot;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.language_code = language_code;
    }

    /**
     * @return the id
     */
    @JsonProperty("id")
    public Integer getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    @JsonProperty("id")
    public void setId(final Integer id)
    {
        this.id = id;
    }

    /**
     * @return the is_bot
     */
    @JsonProperty("is_bot")
    public boolean isIs_bot()
    {
        return is_bot;
    }

    /**
     * @param is_bot the is_bot to set
     */
    @JsonProperty("is_bot")
    public void setIs_bot(final boolean is_bot)
    {
        this.is_bot = is_bot;
    }

    /**
     * @return the first_name
     */
    @JsonProperty("first_name")
    public String getFirst_name()
    {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    @JsonProperty("first_name")
    public void setFirst_name(final String first_name)
    {
        this.first_name = first_name;
    }

    /**
     * @return the last_name
     */
    @JsonProperty("last_name")
    public String getLast_name()
    {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    @JsonProperty("last_name")
    public void setLast_name(final String last_name)
    {
        this.last_name = last_name;
    }

    /**
     * @return the username
     */
    @JsonProperty("username")
    public String getUsername()
    {
        return username;
    }

    /**
     * @param username the username to set
     */
    @JsonProperty("username")
    public void setUsername(final String username)
    {
        this.username = username;
    }

    /**
     * @return the language_code
     */
    @JsonProperty("language_code")
    public String getLanguage_code()
    {
        return language_code;
    }

    /**
     * @param language_code the language_code to set
     */
    @JsonProperty("language_code")
    public void setLanguage_code(final String language_code)
    {
        this.language_code = language_code;
    }

}