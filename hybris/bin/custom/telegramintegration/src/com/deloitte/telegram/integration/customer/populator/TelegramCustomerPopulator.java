/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.customer.populator;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import com.deloitte.telegram.integration.constants.TelegramintegrationConstants;

/**
 *
 */
public class TelegramCustomerPopulator extends CustomerPopulator
{
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Override
    public void populate(final CustomerModel source, final CustomerData target)
    {
        super.populate(source, target);
        if (source != null && source.getTelegramNotification() != null
                && source.getTelegramNotification().booleanValue())
        {
            final String telegramOptInUrl = configurationService.getConfiguration()
                    .getString(TelegramintegrationConstants.TELEGRAM_OPT_IN_URL)
                    + configurationService.getConfiguration().getString(TelegramintegrationConstants.TELEGRAM_BOT_ID)
                    + TelegramintegrationConstants.TELEGRAM_START + source.getTelegramUniqueRequestId();
            target.setTelegramOptInUrl(telegramOptInUrl);
        }
    }

}
