/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *author gs
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ok", "result" })
public class SendMessageResponse
{
    @JsonProperty("ok")
    private boolean ok;

    @JsonProperty("message")
    private List<Result> result;

    /**
     * No args constructor for use in serialization
     *
     */
    public SendMessageResponse()
    {}

    /**
     *@param update_id
     *@param message
     */
    public SendMessageResponse(final boolean ok, final List<Result> result)
    {
        super();
        this.ok = ok;
        this.result = result;
    }

    /**
     * @return the ok
     */
    @JsonProperty("ok")
    public boolean isOk()
    {
        return ok;
    }

    /**
     * @param ok the ok to set
     */
    @JsonProperty("ok")
    public void setOk(final boolean ok)
    {
        this.ok = ok;
    }

    /**
     * @return the result
     */
    @JsonProperty("result")
    public List<Result> getResult()
    {
        return result;
    }

    /**
     * @param result the result to set
     */
    @JsonProperty("result")
    public void setResult(final List<Result> result)
    {
        this.result = result;
    }
}
