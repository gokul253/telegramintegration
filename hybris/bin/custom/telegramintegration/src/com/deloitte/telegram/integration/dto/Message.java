/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *author gs
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "message_id", "from", "chat", "date", "text", "entities" })
public class Message
{
    @JsonProperty("message_id")
    private Integer message_id;

    @JsonProperty("from")
    private From from;

    @JsonProperty("chat")
    private Chat chat;

    @JsonProperty("date")
    private Integer date;

    @JsonProperty("text")
    private String text;

    @JsonProperty("entities")
    private List<Entity> entities;

    /**
     * No args constructor for use in serialization
     *
     */
    public Message()
    {}

    /**
     *@param message_id
     *@param from
     *@param chat
     *@param date
     *@param text
     *@param entities
     */
    public Message(final Integer message_id, final From from, final Chat chat, final Integer date, final String text,
            final List<Entity> entities)
    {
        super();
        this.message_id = message_id;
        this.from = from;
        this.chat = chat;
        this.date = date;
        this.text = text;
        this.entities = entities;
    }

    /**
     * @return the message_id
     */
    @JsonProperty("message_id")
    public Integer getMessage_id()
    {
        return message_id;
    }

    /**
     * @param message_id the message_id to set
     */
    @JsonProperty("message_id")
    public void setMessage_id(final Integer message_id)
    {
        this.message_id = message_id;
    }

    /**
     * @return the from
     */
    @JsonProperty("from")
    public From getFrom()
    {
        return from;
    }

    /**
     * @param from the from to set
     */
    @JsonProperty("from")
    public void setFrom(final From from)
    {
        this.from = from;
    }

    /**
     * @return the chat
     */
    @JsonProperty("chat")
    public Chat getChat()
    {
        return chat;
    }

    /**
     * @param chat the chat to set
     */
    @JsonProperty("chat")
    public void setChat(final Chat chat)
    {
        this.chat = chat;
    }

    /**
     * @return the date
     */
    @JsonProperty("date")
    public Integer getDate()
    {
        return date;
    }

    /**
     * @param date the date to set
     */
    @JsonProperty("date")
    public void setDate(final Integer date)
    {
        this.date = date;
    }

    /**
     * @return the text
     */
    @JsonProperty("text")
    public String getText()
    {
        return text;
    }

    /**
     * @param text the text to set
     */
    @JsonProperty("text")
    public void setText(final String text)
    {
        this.text = text;
    }

    /**
     * @return the entities
     */
    @JsonProperty("entities")
    public List<Entity> getEntities()
    {
        return entities;
    }

    /**
     * @param entities the entities to set
     */
    @JsonProperty("entities")
    public void setEntities(final List<Entity> entities)
    {
        this.entities = entities;
    }

}
