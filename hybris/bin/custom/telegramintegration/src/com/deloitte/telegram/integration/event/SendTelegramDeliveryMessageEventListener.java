/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.event;

import de.hybris.platform.orderprocessing.events.SendDeliveryMessageEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.yacceleratorcore.event.SendDeliveryMessageEventListener;

import javax.annotation.Resource;

import com.deloitte.telegram.integration.service.TelegramintegrationService;

/**
 *
 */
public class SendTelegramDeliveryMessageEventListener extends SendDeliveryMessageEventListener
{
    @Resource(name = "telegramintegrationService")
    private TelegramintegrationService telegramIntegrationService;

    @Override
    protected void onSiteEvent(final SendDeliveryMessageEvent sendDeliveryMessageEvent)
    {
        final ConsignmentModel consignmentModel = sendDeliveryMessageEvent.getProcess().getConsignment();
        final ConsignmentProcessModel consignmentProcessModel = getBusinessProcessService().createProcess(
                "sendDeliveryEmailProcess-" + consignmentModel.getCode() + "-" + System.currentTimeMillis(),
                "sendDeliveryEmailProcess");
        consignmentProcessModel.setConsignment(consignmentModel);
        getModelService().save(consignmentProcessModel);
        getBusinessProcessService().startProcess(consignmentProcessModel);
        telegramIntegrationService.sendOrderNotificationMessage(consignmentModel);
    }
}
