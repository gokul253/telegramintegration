/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *author gs
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "update_id", "message" })
public class Result
{
    @JsonProperty("update_id")
    private Integer update_id;

    @JsonProperty("message")
    private Message message;

    /**
     * No args constructor for use in serialization
     *
     */
    public Result()
    {}

    /**
     *@param update_id
     *@param message
     */
    public Result(final Integer update_id, final Message message)
    {
        super();
        this.update_id = update_id;
        this.message = message;
    }

    /**
     * @return the update_id
     */
    @JsonProperty("update_id")
    public Integer getUpdate_id()
    {
        return update_id;
    }

    /**
     * @param update_id the update_id to set
     */
    @JsonProperty("update_id")
    public void setUpdate_id(final Integer update_id)
    {
        this.update_id = update_id;
    }

    /**
     * @return the message
     */
    @JsonProperty("message")
    public Message getMessage()
    {
        return message;
    }

    /**
     * @param message the message to set
     */
    @JsonProperty("message")
    public void setMessage(final Message message)
    {
        this.message = message;
    }

}
