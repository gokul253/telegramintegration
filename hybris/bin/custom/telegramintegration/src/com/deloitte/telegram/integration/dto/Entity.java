/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *author gs
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "offset", "length", "type", "url" })
public class Entity
{
    @JsonProperty("id")
    private Integer offset;

    @JsonProperty("id")
    private Integer length;

    @JsonProperty("type")
    private String type;

    @JsonProperty("url")
    private String url;

    /**
     * No args constructor for use in serialization
     *
     */
    public Entity()
    {}

    /**
    *
    * @param offset
    * @param length
    * @param type
    */
    public Entity(final Integer offset, final Integer length, final String type, final String url)
    {
        super();
        this.offset = offset;
        this.length = length;
        this.type = type;
        this.url = url;
    }

    /**
     * @return the offset
     */
    @JsonProperty("offset")
    public Integer getOffset()
    {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    @JsonProperty("offset")
    public void setOffset(final Integer offset)
    {
        this.offset = offset;
    }

    /**
     * @return the length
     */
    @JsonProperty("length")
    public Integer getLength()
    {
        return length;
    }

    /**
     * @param length the length to set
     */
    @JsonProperty("length")
    public void setLength(final Integer length)
    {
        this.length = length;
    }

    /**
     * @return the type
     */
    @JsonProperty("type")
    public String getType()
    {
        return type;
    }

    /**
     * @param type the type to set
     */
    @JsonProperty("type")
    public void setType(final String type)
    {
        this.type = type;
    }

    /**
     * @return the url
     */
    @JsonProperty("url")
    public String getUrl()
    {
        return url;
    }

    /**
     * @param url the url to set
     */
    @JsonProperty("url")
    public void setUrl(final String url)
    {
        this.url = url;
    }

}
