/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.job;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.time.TimeService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.deloitte.telegram.integration.model.TelegramAbandonedCartNotificationCronJobModel;
import com.deloitte.telegram.integration.service.TelegramintegrationService;

/**
 *author gs
 */
public class TelegramAbandonedCartNotificationJob
        extends AbstractJobPerformable<TelegramAbandonedCartNotificationCronJobModel>
{

    private static final Logger LOG = Logger.getLogger(TelegramAbandonedCartNotificationJob.class);

    @Resource(name = "commerceCartDao")
    private CommerceCartDao commerceCartDao;

    @Resource(name = "timeService")
    private TimeService timeService;

    @Resource(name = "telegramintegrationService")
    private TelegramintegrationService telegramIntegrationService;

    private static final int DEFAULT_CART_MAX_AGE = 604800; //7 days

    @Override
    public PerformResult perform(final TelegramAbandonedCartNotificationCronJobModel job)
    {
        try
        {
            if (job.getSites() == null || job.getSites().isEmpty())
            {
                LOG.warn("There is no sites defined for " + job.getCode());
                return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
            }
            final int cartAge = job.getDefaultCartMaxAge() != null ? job.getDefaultCartMaxAge().intValue()
                    : DEFAULT_CART_MAX_AGE;
            for (final BaseSiteModel site : job.getSites())
            {
                for (final CartModel cart : commerceCartDao.getCartsForRemovalForSiteAndUser(
                        new DateTime(timeService.getCurrentTime()).minusSeconds(cartAge).toDate(), site, null))
                {
                    telegramIntegrationService.sendAbandonedCartNotification(cart);
                }
            }
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }
        catch (final Exception e)
        {
            LOG.error("Exception occurred during cart cleanup", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }
    }

}
