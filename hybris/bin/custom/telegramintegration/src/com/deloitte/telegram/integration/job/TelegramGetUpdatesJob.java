/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.deloitte.telegram.integration.service.TelegramintegrationService;

/**
 *author gs
 */
public class TelegramGetUpdatesJob extends AbstractJobPerformable<CronJobModel>
{
    @Resource(name = "telegramintegrationService")
    private TelegramintegrationService telegramIntegrationService;

    /** The logger. */
    private static Logger log = Logger.getLogger(TelegramGetUpdatesJob.class);

    @Override
    public PerformResult perform(final CronJobModel job)
    {
        try
        {
            log.debug("TelegramGetUpdatesJob : START");
            telegramIntegrationService.updateCustomersWithTelegramChatId();
            log.debug("TelegramGetUpdatesJob : END");
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }
        catch (final Exception ex)
        {
            log.error("TelegramGetUpdatesJob : ERROR ", ex);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }
    }

}
