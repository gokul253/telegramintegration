/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *author gs
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "first_name", "last_name", "username", "type" })
public class Chat
{
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("first_name")
    private String first_name;

    @JsonProperty("last_name")
    private String last_name;

    @JsonProperty("username")
    private String username;

    @JsonProperty("type")
    private String type;

    /**
     * No args constructor for use in serialization
     *
     */
    public Chat()
    {}

    /**
    *
    * @param id
    * @param first_name
    * @param last_name
    * @param username
    * @param type
    */
    public Chat(final Integer id, final String first_name, final String last_name, final String username,
            final String type)
    {
        super();
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.type = type;
    }

    /**
     * @return the id
     */
    @JsonProperty("id")
    public Integer getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    @JsonProperty("id")
    public void setId(final Integer id)
    {
        this.id = id;
    }

    /**
     * @return the first_name
     */
    @JsonProperty("first_name")
    public String getFirst_name()
    {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    @JsonProperty("first_name")
    public void setFirst_name(final String first_name)
    {
        this.first_name = first_name;
    }

    /**
     * @return the last_name
     */
    @JsonProperty("last_name")
    public String getLast_name()
    {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    @JsonProperty("last_name")
    public void setLast_name(final String last_name)
    {
        this.last_name = last_name;
    }

    /**
     * @return the username
     */
    @JsonProperty("username")
    public String getUsername()
    {
        return username;
    }

    /**
     * @param username the username to set
     */
    @JsonProperty("username")
    public void setUsername(final String username)
    {
        this.username = username;
    }

    /**
     * @return the username
     */
    @JsonProperty("type")
    public String getType()
    {
        return type;
    }

    /**
     * @param username the username to set
     */
    @JsonProperty("type")
    public void setType(final String type)
    {
        this.type = type;
    }
}
