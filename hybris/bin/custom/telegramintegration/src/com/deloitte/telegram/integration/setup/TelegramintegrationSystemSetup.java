/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.deloitte.telegram.integration.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import com.deloitte.telegram.integration.constants.TelegramintegrationConstants;

@SystemSetup(extension = TelegramintegrationConstants.EXTENSIONNAME)
public class TelegramintegrationSystemSetup extends AbstractSystemSetup
{

    @Override
    public List<SystemSetupParameter> getInitializationOptions()
    {
        // XXX Auto-generated method stub
        return null;
    }

    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext context)
    {
        importImpexFile(context, "/telegramintegration/import/common/telegramcronjobs.impex");
    }
}
