/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.deloitte.telegram.integration.rest.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

/**
 *author gs
 */
public class TelegramRestClient
{

    /**
     *
     */
    private static final String DOCUMENT = "document";

    private static final Logger LOG = Logger.getLogger(TelegramRestClient.class);

    private String url;

    private int connectionTimeOut;

    private int readTimeOut;

    private String authToken;

    private static final String BLANK = "";

    /**
     * Do request.
     *
     * @param method the method
     * @param action the action
     * @param params the request parameters
     * @return the String response
     */
    public String doRequest(final String method, final String action, final Map<String, Object> params)
    {
        HttpsURLConnection connection = null;

        try
        {
            // Establish connectivity
            final URL urlObject = new URL(String.join(BLANK, getUrl(), getAuthToken(), action));
            connection = (HttpsURLConnection) urlObject.openConnection();

            // Set timeouts
            connection.setConnectTimeout(getConnectionTimeOut());
            connection.setReadTimeout(getReadTimeOut());

            // Disable caching
            connection.setUseCaches(false);

            // Set request method
            connection.setRequestMethod(method);

            // Enable writing to resource and disable any user interactions
            connection.setDoOutput(true);
            connection.setAllowUserInteraction(false);

            //Send the request parameters
            if (params != null)
            {
                final StringBuilder postData = new StringBuilder();
                for (final Map.Entry<String, Object> item : params.entrySet())
                {
                    if (postData.length() != 0)
                    {
                        postData.append('&');
                    }
                    postData.append(URLEncoder.encode(item.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(item.getValue()), "UTF-8"));
                }
                final byte[] postDataBytes = postData.toString().getBytes("UTF-8");
                connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                connection.getOutputStream().write(postDataBytes);
            }

            // Retrieve the response code and message
            final int responseCode = connection.getResponseCode();
            final String message = connection.getResponseMessage();

            // Log response code and message
            if (!Arrays
                    .asList(Integer.valueOf(HttpURLConnection.HTTP_OK), Integer.valueOf(HttpURLConnection.HTTP_CREATED))
                    .contains(Integer.valueOf(responseCode)))
            {
                LOG.error(
                        "The server returned with the response code " + responseCode + " and with message " + message);
            }
            else
            {
                LOG.info("The server returned with the response code " + responseCode + " and with message " + message);
            }

            // Read response
            final BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(responseCode < HttpURLConnection.HTTP_BAD_REQUEST
                            ? connection.getInputStream() : connection.getErrorStream()));
            String inputLine;
            final StringBuilder response = new StringBuilder();

            while ((inputLine = bufferedReader.readLine()) != null)
            {
                response.append(inputLine);
            }

            bufferedReader.close();

            // Log response message
            if (LOG.isDebugEnabled())
            {
                LOG.debug("Response : " + response.toString());
            }

            return response.toString();
        }
        catch (final Exception e)
        {
            LOG.error("Exception occured", e);
            return StringUtils.EMPTY;
        }
        finally
        {
            if (connection != null)
            {
                connection.disconnect();
            }
        }
    }

    /**
     * @throws IOException
     * @throws ClientProtocolException
    *
    */
    public void doRequest(final String method, final String action, final Map<String, Object> params, final File file)
    {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        URL urlObject = null;
        CloseableHttpResponse response = null;
        try
        {
            urlObject = new URL(String.join(BLANK, getUrl(), getAuthToken(), action));
            final HttpPost httppost = new HttpPost(urlObject.toString());
            final MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            if (MapUtils.isNotEmpty(params))
            {
                for (final Map.Entry<String, Object> item : params.entrySet())
                {
                    builder.addTextBody(item.getKey(), item.getValue().toString());
                }
            }
            builder.addBinaryBody(DOCUMENT, new FileInputStream(file), ContentType.APPLICATION_OCTET_STREAM,
                    file.getName());
            final HttpEntity multipart = builder.build();
            httppost.setEntity(multipart);
            response = httpClient.execute(httppost);
            final HttpEntity responseEntity = response.getEntity();
        }
        catch (final ClientProtocolException e)
        {
            LOG.error("error while sending request");
        }
        catch (final IOException e)
        {
            LOG.error("Something went wrong with file");
        }

    }

    /**
    * @return the url
    */
    public String getUrl()
    {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(final String url)
    {
        this.url = url;
    }

    /**
     * @return the connectionTimeOut
     */
    public int getConnectionTimeOut()
    {
        return connectionTimeOut;
    }

    /**
     * @param connectionTimeOut the connectionTimeOut to set
     */
    public void setConnectionTimeOut(final int connectionTimeOut)
    {
        this.connectionTimeOut = connectionTimeOut;
    }

    /**
     * @return the readTimeOut
     */
    public int getReadTimeOut()
    {
        return readTimeOut;
    }

    /**
     * @param readTimeOut the readTimeOut to set
     */
    public void setReadTimeOut(final int readTimeOut)
    {
        this.readTimeOut = readTimeOut;
    }

    /**
     * @return the authToken
     */
    public String getAuthToken()
    {
        return authToken;
    }

    /**
     * @param authToken the authToken to set
     */
    public void setAuthToken(final String authToken)
    {
        this.authToken = authToken;
    }

}
