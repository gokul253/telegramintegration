package com.deloitte.telegram.builder;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPTableEvent;
import com.itextpdf.text.pdf.PdfWriter;

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */

public class TelegramPDFBuilder
{
    private static final Logger LOG = LoggerFactory.getLogger(TelegramPDFBuilder.class);

    private static final String CONTEXT_URL = "context.url";

    @Resource
    private ConfigurationService configurationService;

    public File buildPDF(final String filename, final AbstractOrderModel order)
        throws FileNotFoundException, DocumentException
    {
        final Document document = new Document();
        final File file = new File(filename);
        PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();

        final Font fontStyle_Bold = FontFactory.getFont(FontFactory.COURIER, 14f, Font.BOLD);
        final float lineSpacing = 15f;
        final Font fontSize_12 = FontFactory.getFont(null, 12f);

        document.add(new Paragraph("Dear " + order.getUser().getName() + ",", fontStyle_Bold));
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);

        final StringBuilder docbody1 = new StringBuilder("Thank you for your order. Your order number is "
                + order.getCode() + " and the total cost was " + order.getCurrency().getSymbol() + order.getTotalPrice()
                + ". This doc contains a complete summary of your order. Please retain this confirmation for your records.");
        document.add(new Paragraph(new Phrase(lineSpacing, docbody1.toString(), fontSize_12)));

        document.add(createDeliveryAndPaymentInfoTable(order));
        document.add(createItemsInfoTable(order, "ITEMS TO BE DELIVERED"));
        document.add(createPriceInfoTable(order));

        final StringBuilder docbody2 = new StringBuilder(
                "If you've registered an account with us, the details of your order can also be found in your Order History.");
        final Font fontSize_9 = FontFactory.getFont(null, 9f);
        document.add(new Paragraph(new Phrase(lineSpacing, docbody2.toString(), fontSize_9)));
        document.add(Chunk.NEWLINE);

        final StringBuilder docbody3 = new StringBuilder(
                "If you do not have an account, why not create an account today? You will check out faster next time by using saved delivery address and payment detai");
        document.add(new Paragraph(new Phrase(lineSpacing, docbody3.toString(), fontSize_9)));
        document.add(Chunk.NEWLINE);

        final StringBuilder docbody4 = new StringBuilder(
                "Please see the Delivery Information for further details about receiving your order.");
        document.add(new Paragraph(new Phrase(lineSpacing, docbody4.toString(), fontSize_9)));
        document.add(Chunk.NEWLINE);

        final StringBuilder docbody5 = new StringBuilder(
                "PIf we can help you with any enquiry, please use our Contact Us page, or contact our customer services team directly via phone +44 (0)20 / 7429 4175 or email customerservices@hybris.com.");
        document.add(new Paragraph(new Phrase(lineSpacing, docbody5.toString(), fontSize_9)));
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);

        final StringBuilder docbody6 = new StringBuilder("Many Thanks");
        document.add(new Paragraph(new Phrase(lineSpacing, docbody6.toString(), fontSize_9)));

        final StringBuilder docbody7 = new StringBuilder("Customer Services");
        document.add(new Paragraph(new Phrase(lineSpacing, docbody7.toString(), fontSize_9)));

        document.close();
        return file;

    }

    /**
     * creates table having all details
     */
    private PdfPTable createPriceInfoTable(final AbstractOrderModel order)
    {
        final PdfPTable table = new PdfPTable(3);
        table.setTotalWidth(520);
        table.setSpacingBefore(10);
        table.setSpacingAfter(10);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(true);
        table.setTableEvent(new BorderEvent());

        PdfPCell cell;
        final Font fontStyle_Bold_Totals = FontFactory.getFont(FontFactory.COURIER, 13f, Font.BOLD);
        final Font fontStyle_Bold = FontFactory.getFont(FontFactory.COURIER, 11f, Font.BOLD);
        final Font fontSize_11 = FontFactory.getFont(FontFactory.COURIER, 11f);
        final String currencySymbol = order.getCurrency().getSymbol();

        cell = new PdfPCell(new Phrase("", fontStyle_Bold));
        cell.setRowspan(6);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(130f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Order totals", fontStyle_Bold_Totals));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("", fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("SubTotal:", fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                " " + currencySymbol + (null != order.getSubtotal() ? order.getSubtotal().toString() : "0.00"),
                fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Order discounts:", fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                "-" + currencySymbol
                        + (null != order.getTotalDiscounts() ? order.getTotalDiscounts().toString() : "0.00"),
                fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Delivery:", fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                " " + currencySymbol + (null != order.getDeliveryCost() ? order.getDeliveryCost().toString() : "0.00"),
                fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total:", fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(30f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                " " + currencySymbol + (null != order.getTotalPrice() ? order.getTotalPrice().toString() : "0.00"),
                fontStyle_Bold_Totals));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(30f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                "Your order includes " + currencySymbol
                        + (null != order.getTotalTax() ? order.getTotalTax().toString() : "0.00") + " tax",
                fontSize_11));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        cell.setFixedHeight(30f);
        table.addCell(cell);

        return table;
    }

    /**
     * creates table having all details of items purchased
     * @throws IOException
     * @throws MalformedURLException
     * @throws BadElementException
     */
    private PdfPTable createItemsInfoTable(final AbstractOrderModel order, final String tableHead)
    {
        final PdfPTable table = new PdfPTable(11);
        table.setTotalWidth(520);
        table.setSpacingBefore(10);
        table.setSpacingAfter(10);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(true);
        table.setTableEvent(new BorderEvent());

        PdfPCell cell;
        final Font fontStyle_Bold = FontFactory.getFont(FontFactory.COURIER, 12f, Font.BOLD);
        final Font fontSize_11 = FontFactory.getFont(FontFactory.COURIER, 11f);
        final String currencySymbol = order.getCurrency().getSymbol();

        cell = new PdfPCell(new Phrase(tableHead, fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setColspan(11);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("", fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setColspan(5);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Quantity", fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setColspan(2);
        cell.setPaddingLeft(15f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Item Price", fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total", fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        for (final AbstractOrderEntryModel entry : order.getEntries())
        {
            final String imageUrl = entry.getProduct().getThumbnail().getURL();
            if (null != imageUrl)
            {
                URL url;
                Image image = null;
                try
                {
                    handleSSL();
                    url = new URL(configurationService.getConfiguration().getString(CONTEXT_URL) + imageUrl);
                    final URLConnection con = url.openConnection();
                    image = Image.getInstance(url);
                }
                catch (final Exception e)
                {
                    LOG.error("Error occured while inserting product image");
                }

                cell = new PdfPCell(image);
                cell.setFixedHeight(30f);
                cell.setPaddingBottom(2f);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setVerticalAlignment(Element.ALIGN_CENTER);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            }
            else
            {
                cell = new PdfPCell(new Phrase(""));
            }
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(entry.getProduct().getName(), fontSize_11));
            cell.setColspan(4);
            cell.setPaddingLeft(15f);
            cell.setPaddingBottom(6f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(entry.getQuantity().toString(), fontSize_11));
            cell.setColspan(2);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(currencySymbol + entry.getBasePrice().toString(), fontSize_11));
            cell.setColspan(2);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(currencySymbol + entry.getTotalPrice().toString(), fontSize_11));
            cell.setColspan(2);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
        }
        return table;
    }

    /**
     * creates table having all details of delivery and payment
     */
    private PdfPTable createDeliveryAndPaymentInfoTable(final AbstractOrderModel order)
    {
        final PdfPTable table = new PdfPTable(3);
        table.setTotalWidth(520);
        table.setSpacingBefore(20);
        table.setSpacingAfter(10);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(true);
        table.setTableEvent(new BorderEvent());

        PdfPCell cell;
        final Font fontStyle_Bold = FontFactory.getFont(FontFactory.COURIER, 12f, Font.BOLD);
        final Font fontSize_11 = FontFactory.getFont(FontFactory.COURIER, 11f);

        cell = new PdfPCell(new Phrase("Delivery Address", fontStyle_Bold));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(10f);
        cell.setFixedHeight(20f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Delivery Options", fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Payment Details", fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(getAddress(order.getDeliveryAddress()), fontSize_11));
        cell.setRowspan(3);
        cell.setFixedHeight(180f);
        cell.setPaddingLeft(10f);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(null != order.getDeliveryMode() ? order.getDeliveryMode().getName() : "Standard",
                fontSize_11));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setFixedHeight(20f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                (null != order.getPaymentMode() ? "Paid by " + order.getPaymentMode().getCode() : "credit card"),
                fontSize_11));
        cell.setFixedHeight(55f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                "Your delivery should arrive in " + (null != order.getDeliveryMode()
                        ? order.getDeliveryMode().getDescription() : "3-5 business days") + " following dispatch",
                fontSize_11));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setFixedHeight(160f);
        cell.setRowspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Billing Address", fontStyle_Bold));
        cell.setFixedHeight(20f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(
                getAddress(null != order.getPaymentInfo() ? order.getPaymentInfo().getBillingAddress() : null),
                fontSize_11));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setFixedHeight(130f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        return table;
    }

    /**
     * method to populate all address details
     */
    private String getAddress(final AddressModel addressModel)
    {
        final StringBuilder address = new StringBuilder();
        if (null != addressModel)
        {
            if (StringUtils.isNotBlank(addressModel.getFirstname()))
            {
                address.append(addressModel.getTitle().getCode() + " " + addressModel.getFirstname() + " "
                        + addressModel.getLastname() + "\n");
            }
            if (StringUtils.isNotBlank(addressModel.getLine1()))
            {
                address.append(addressModel.getLine1() + "\n");
            }
            if (StringUtils.isNotBlank(addressModel.getLine2()))
            {
                address.append(addressModel.getLine2() + "\n");
            }
            if (StringUtils.isNotBlank(addressModel.getTown()))
            {
                address.append(addressModel.getTown() + "\n");
            }
            if (StringUtils.isNotBlank(addressModel.getRegion() != null ? addressModel.getRegion().getName() : ""))
            {
                address.append(addressModel.getRegion().getName() + "\n");

            }
            if (StringUtils.isNotBlank(addressModel.getPostalcode()))
            {
                address.append(addressModel.getPostalcode() + "\n");
            }
            if (StringUtils.isNotBlank(addressModel.getCountry() != null ? addressModel.getCountry().getIsocode() : ""))
            {
                address.append(addressModel.getCountry().getIsocode());
            }

        }
        return address.toString();

    }

    /**
     * build pdf for abandoned cart notification.
     * @throws DocumentException
     * @throws FileNotFoundException
     */
    public File buildPDFForAbandonedCartNotification(final String filename, final CartModel cart)
        throws FileNotFoundException, DocumentException
    {

        final Document document = new Document();
        final File file = new File(filename);
        PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();

        final Font fontStyle_Bold = FontFactory.getFont(FontFactory.COURIER, 14f, Font.BOLD);
        final Font fontSize_14 = FontFactory.getFont(null, 14f);

        document.add(new Paragraph("Dear " + cart.getUser().getName() + ",", fontStyle_Bold));
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);

        final Chunk chunk5 = new Chunk(
                "We noticed that you added one or more items to your shopping cart, but didn't continue to checkout. When you are ready to buy, simply visit your shopping cart to complete your order.",
                fontSize_14);

        final Paragraph p = new Paragraph();
        p.add(chunk5);

        document.add(p);
        document.add(createItemsInfoTable(cart, "CART DETAILS"));
        document.add(createPriceInfoTable(cart));

        document.close();
        return file;
    }

    private static void handleSSL()
    {
        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager()
        {
            public X509Certificate[] getAcceptedIssuers()
            {
                return null;
            }

            public void checkClientTrusted(final X509Certificate[] certs, final String authType)
            {
            }

            public void checkServerTrusted(final X509Certificate[] certs, final String authType)
            {
            }
        } };

        // Install the all-trusting trust manager
        try
        {
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }

        // Create all-trusting host name verifier
        final HostnameVerifier allHostsValid = new HostnameVerifier()
        {
            public boolean verify(final String hostname, final SSLSession session)
            {
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    public class BorderEvent implements PdfPTableEvent
    {
        public void tableLayout(final PdfPTable table, final float[][] widths, final float[] heights,
                final int headerRows, final int rowStart, final PdfContentByte[] canvases)
        {
            final float width[] = widths[0];
            final float x1 = width[0];
            final float x2 = width[width.length - 1];
            final float y1 = heights[0];
            final float y2 = heights[heights.length - 1];
            final PdfContentByte cb = canvases[PdfPTable.LINECANVAS];
            cb.rectangle(x1, y1, x2 - x1, y2 - y1);
            cb.stroke();
            cb.resetRGBColorStroke();
        }
    }

}
