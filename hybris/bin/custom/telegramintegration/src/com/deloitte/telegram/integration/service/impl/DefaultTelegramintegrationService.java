/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.deloitte.telegram.integration.service.impl;

import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.deloitte.telegram.builder.TelegramPDFBuilder;
import com.deloitte.telegram.integration.constants.TelegramintegrationConstants;
import com.deloitte.telegram.integration.dto.GetUpdatesResponse;
import com.deloitte.telegram.integration.dto.Message;
import com.deloitte.telegram.integration.dto.Result;
import com.deloitte.telegram.integration.rest.client.TelegramRestClient;
import com.deloitte.telegram.integration.service.TelegramintegrationService;
import com.google.gson.Gson;
import com.itextpdf.text.DocumentException;

/**
 *author gs
 */

public class DefaultTelegramintegrationService implements TelegramintegrationService
{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTelegramintegrationService.class);

    private static final String DOC_FORMAT = ".pdf";

    private static final String PDF_DOCUMENT_PATH = "pdf.document.path";

    private static final String DELIMETER_SPACE = " ";

    private static final int SPLIT_LIMIT = 2;

    private static final String CHAT_ID = "chat_id";

    private static final String PARSE_MODE = "parse_mode";

    private static final String TEXT = "text";

    private static final String HTML = "HTML";

    private static final String DOCUMENT = "document";

    private static final String FIND_CUSTOMER_BY_TELEGRAM_UNIQUEID = "SELECT {" + CustomerModel.PK + "} FROM {"
            + CustomerModel._TYPECODE + "} WHERE {" + CustomerModel.TELEGRAMUNIQUEREQUESTID + "} = ?uniqueId AND {"
            + CustomerModel.TELEGRAMCHATID + "} IS NULL AND {" + CustomerModel.TELEGRAMNOTIFICATION + "} = 1";

    private MediaService mediaService;

    private ModelService modelService;

    private FlexibleSearchService flexibleSearchService;

    @Resource(name = "telegramRestClient")
    private TelegramRestClient telegramRestClient;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    private TelegramPDFBuilder pdfBuilder;

    @Override
    public String getHybrisLogoUrl(final String logoCode)
    {
        final MediaModel media = mediaService.getMedia(logoCode);

        // Keep in mind that with Slf4j you don't need to check if debug is enabled, it is done under the hood.
        LOG.debug("Found media [code: {}]", media.getCode());

        return media.getURL();
    }

    @Override
    public void createLogo(final String logoCode)
    {
        final Optional<CatalogUnawareMediaModel> existingLogo = findExistingLogo(logoCode);

        final CatalogUnawareMediaModel media = existingLogo.isPresent() ? existingLogo.get()
                : modelService.create(CatalogUnawareMediaModel.class);
        media.setCode(logoCode);
        media.setRealFileName("sap-hybris-platform.png");
        modelService.save(media);

        mediaService.setStreamForMedia(media, getImageStream());
    }

    private final static String FIND_LOGO_QUERY = "SELECT {" + CatalogUnawareMediaModel.PK + "} FROM {"
            + CatalogUnawareMediaModel._TYPECODE + "} WHERE {" + CatalogUnawareMediaModel.CODE + "}=?code";

    private Optional<CatalogUnawareMediaModel> findExistingLogo(final String logoCode)
    {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_LOGO_QUERY);
        fQuery.addQueryParameter("code", logoCode);

        try
        {
            return Optional.of(flexibleSearchService.searchUnique(fQuery));
        }
        catch (final SystemException e)
        {
            return Optional.empty();
        }
    }

    private InputStream getImageStream()
    {
        return DefaultTelegramintegrationService.class
                .getResourceAsStream("/telegramintegration/sap-hybris-platform.png");
    }

    @Override
    public void updateCustomersWithTelegramChatId()
    {
        final String responseJson = telegramRestClient.doRequest(TelegramintegrationConstants.HTTPS_POST,
                TelegramintegrationConstants.TELEGRAM_GET_UPDATES, null);
        if (responseJson != null)
        {
            final Gson gson = new Gson();
            final GetUpdatesResponse getUpdates = gson.fromJson(responseJson, GetUpdatesResponse.class);
            if (getUpdates.isOk())
            {
                for (final Result result : getUpdates.getResult())
                {
                    updateCustomerWithChatIdAndSendRegistrationMessage(result.getMessage());
                }
            }
        }
    }

    /**
     *
     */
    private void updateCustomerWithChatIdAndSendRegistrationMessage(final Message message)
    {
        final String text = message.getText();
        final String[] textArray = text.split(DELIMETER_SPACE, SPLIT_LIMIT);
        if (textArray.length == 2)
        {
            final String uniqueId = textArray[1];
            try
            {
                final CustomerModel customer = getCustomerForUniqueId(uniqueId);
                if (message.getChat() != null && message.getChat().getId() != null)
                {
                    final int chatId = message.getChat().getId().intValue();
                    customer.setTelegramChatId(new String(
                            Base64.encodeBase64(String.valueOf(chatId).getBytes(StandardCharsets.US_ASCII))));
                    modelService.save(customer);
                    sendRegistrationSuccessMessage(customer);
                }
            }
            catch (final Exception ex)
            {
                LOG.error(ex.getMessage());
            }
        }
    }

    private CustomerModel getCustomerForUniqueId(final String uniqueId)
    {
        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("uniqueId", uniqueId);
        return flexibleSearchService
                .searchUnique(new FlexibleSearchQuery(FIND_CUSTOMER_BY_TELEGRAM_UNIQUEID, queryParams));
    }

    @Override
    public void sendOrderConfirmationMessage(final OrderModel order)
    {
        final CustomerModel customer = (CustomerModel) order.getUser();
        if (customer.getTelegramNotification() != null && getDecodedTelegramChatId(customer) != null)
        {
            final String chatId = getDecodedTelegramChatId(customer);
            final StringBuilder text = new StringBuilder();
            text.append("<b>Customer Id:</b> ");
            text.append(customer.getUid());
            text.append(",<b>Order Number:</b> ");
            text.append(order.getCode());
            text.append(", <b>Products Placed in Order:</b> ");
            for (final AbstractOrderEntryModel entry : order.getEntries())
            {
                text.append("Product " + entry.getEntryNumber() + ": ");
                text.append(entry.getProduct().getCode() + " ");
            }
            text.append(", <b>Order Total:</b> ");
            text.append(order.getTotalPrice());
            final Map<String, Object> params = new HashMap<>();
            params.put(CHAT_ID, chatId);
            params.put(PARSE_MODE, HTML);
            params.put(TEXT, text.toString());
            telegramRestClient.doRequest(TelegramintegrationConstants.HTTPS_POST,
                    TelegramintegrationConstants.TELEGRAM_SEND_MESSAGE, params);
            order.setTelegramNotificationSent(true);
            modelService.save(order);
        }
    }

    /**
     *
     */
    private String getDecodedTelegramChatId(final CustomerModel customer)
    {
        return new String(
                Base64.decodeBase64(customer.getTelegramChatId().toString().getBytes(StandardCharsets.US_ASCII)));
    }

    private void sendRegistrationSuccessMessage(final CustomerModel customer)
    {
        final String chatId = getDecodedTelegramChatId(customer);
        final StringBuilder text = new StringBuilder();
        text.append("<b>Customer Id :</b> ");
        text.append(customer.getUid());
        text.append(" : Registered Successfully! Kindly login to place orders.");
        final Map<String, Object> params = new HashMap<>();
        params.put(CHAT_ID, chatId);
        params.put(PARSE_MODE, HTML);
        params.put(TEXT, text.toString());
        telegramRestClient.doRequest(TelegramintegrationConstants.HTTPS_POST,
                TelegramintegrationConstants.TELEGRAM_SEND_MESSAGE, params);
    }

    public void sendAbandonedCartNotification(final CartModel cart)
    {
        final CustomerModel customer = (CustomerModel) cart.getUser();
        if (customer.getTelegramNotification() != null && getDecodedTelegramChatId(customer) != null)
        {

            File file = null;
            try
            {
                file = pdfBuilder.buildPDFForAbandonedCartNotification(
                        getConfigurationService().getConfiguration().getString(PDF_DOCUMENT_PATH) + cart.getCode()
                                + DOC_FORMAT,
                        cart);
            }
            catch (FileNotFoundException | DocumentException e)
            {
                LOG.error("Unable to generate order details pdf", e.getMessage());
            }

            final String chatId = getDecodedTelegramChatId(customer);
            final Map<String, Object> params = new HashMap<>();
            params.put(CHAT_ID, chatId);
            telegramRestClient.doRequest(TelegramintegrationConstants.HTTPS_POST,
                    TelegramintegrationConstants.TELEGRAM_SEND_DOCUMENT, params, file);
            associateFileToAbstractOrderAndDelete(cart, file);
        }
    }

    @Override
    public void sendOrderDetailsDocument(final AbstractOrderModel order)
    {
        File file = null;
        try
        {
            file = pdfBuilder.buildPDF(getConfigurationService().getConfiguration().getString(PDF_DOCUMENT_PATH)
                    + order.getCode() + DOC_FORMAT, order);
        }
        catch (final FileNotFoundException | DocumentException e)
        {
            LOG.error("Unable to generate order details pdf", e.getCause());
        }
        final Map<String, Object> params = new HashMap<>();
        final CustomerModel customer = ((CustomerModel) order.getUser());

        if (BooleanUtils.isTrue(customer.getTelegramNotification())
                && StringUtils.isNotBlank(getDecodedTelegramChatId(customer)))
        {
            params.put(CHAT_ID, getDecodedTelegramChatId(customer));
            telegramRestClient.doRequest(TelegramintegrationConstants.HTTPS_POST,
                    TelegramintegrationConstants.TELEGRAM_SEND_DOCUMENT, params, file);
            order.setTelegramNotificationSent(true);
            modelService.save(order);
            associateFileToAbstractOrderAndDelete(order, file);
        }
        else
        {
            LOG.error("either customer has not opt in for telegram notification or chat Id is not configured yet");
        }
    }

    /**
    *
    */
    private void associateFileToAbstractOrderAndDelete(final AbstractOrderModel abstractOrder, final File file)
    {
        final List<MediaModel> pdfNotifications = new ArrayList<>();
        if (abstractOrder.getTelegramPdfNotifications() != null
                && CollectionUtils.isNotEmpty(abstractOrder.getTelegramPdfNotifications()))
        {
            pdfNotifications.addAll(abstractOrder.getTelegramPdfNotifications());
        }
        try
        {
            final MultipartFile multipartFile = new MockMultipartFile(file.getName(), new FileInputStream(file));
            final InputStream fileInputStream = multipartFile.getInputStream();
            final CatalogUnawareMediaModel mediaModel = modelService.create(CatalogUnawareMediaModel.class);
            mediaModel.setCode(System.currentTimeMillis() + "_" + file.getName());
            modelService.save(mediaModel);
            mediaService.setStreamForMedia(mediaModel, fileInputStream, file.getName(), multipartFile.getContentType());
            modelService.refresh(mediaModel);
            pdfNotifications.add(mediaModel);
            abstractOrder.setTelegramPdfNotifications(pdfNotifications);
            modelService.save(abstractOrder);
            file.delete();
        }
        catch (final IOException e)
        {
            LOG.error("Unable to save PDF notificaiton to Order : " + abstractOrder.getCode());
        }

    }

    @Required
    public void setMediaService(final MediaService mediaService)
    {
        this.mediaService = mediaService;
    }

    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Override
    public void sendOrderNotificationMessage(final ConsignmentModel consignment)
    {
        final CustomerModel customer = (CustomerModel) consignment.getOrder().getUser();
        if (customer.getTelegramNotification() != null && getDecodedTelegramChatId(customer) != null)
        {
            final String chatId = getDecodedTelegramChatId(customer);
            final StringBuilder text = new StringBuilder();
            text.append("Dear ");
            text.append(customer.getName()).append(",");
            text.append(
                    "\n\n Thank you for shopping with us. We would like to provide you with an update on your recent purchase.");
            text.append("\n\n The following item(s) from Order ").append(consignment.getOrder().getCode())
                    .append(" have been shipped.\n");
            for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries())
            {
                text.append("\n Product : " + entry.getOrderEntry().getProduct().getName());
            }
            final Map<String, Object> params = new HashMap<>();
            params.put(CHAT_ID, chatId);
            params.put(PARSE_MODE, HTML);
            params.put(TEXT, text.toString());
            telegramRestClient.doRequest(TelegramintegrationConstants.HTTPS_POST,
                    TelegramintegrationConstants.TELEGRAM_SEND_MESSAGE, params);
        }
    }

    @Override
    public void sendGiveAwayCouponMessage(final List<CouponData> giftCoupons, final OrderModel order)
    {
        final CustomerModel customer = (CustomerModel) order.getUser();
        if (customer.getTelegramNotification() != null && getDecodedTelegramChatId(customer) != null)
        {
            final String chatId = getDecodedTelegramChatId(customer);
            final StringBuilder text = new StringBuilder();
            text.append("Congratulations ");
            text.append(customer.getName()).append(",");
            text.append(
                    "\n\n Thank you for shopping with us. You've earned the following coupons for your recent order ")
                    .append(order.getCode()).append(".");
            for (final CouponData coupon : giftCoupons)
            {
                text.append("\n\n <b>" + coupon.getName() + "</b>");
                text.append("\n Use coupon code: <b>").append(coupon.getCouponCode()).append("</b>");
                final SimpleDateFormat simpleformat = new SimpleDateFormat("MM/dd/yy");
                if (null == coupon.getStartDate() && null != coupon.getEndDate())
                {
                    text.append("\n Valid until : ").append(simpleformat.format(coupon.getEndDate()));
                }
                else if (null != coupon.getStartDate() && null == coupon.getEndDate())
                {
                    text.append("\n Valid from : ").append(simpleformat.format(coupon.getStartDate()));
                }
                else if (null != coupon.getStartDate() && null != coupon.getEndDate())
                {
                    text.append("\n Valid from : ").append(simpleformat.format(coupon.getStartDate()))
                            .append(" until : ").append(simpleformat.format(coupon.getEndDate()));
                }
            }
            final Map<String, Object> params = new HashMap<>();
            params.put(CHAT_ID, chatId);
            params.put(PARSE_MODE, HTML);
            params.put(TEXT, text.toString());
            telegramRestClient.doRequest(TelegramintegrationConstants.HTTPS_POST,
                    TelegramintegrationConstants.TELEGRAM_SEND_MESSAGE, params);
        }
    }

    /**
     * @return the configurationService
     */
    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    /**
     * @return the pdfBuilder
     */
    protected TelegramPDFBuilder getPdfBuilder()
    {
        return pdfBuilder;
    }

    /**
     * @param pdfBuilder the pdfBuilder to set
     */
    @Required
    public void setPdfBuilder(final TelegramPDFBuilder pdfBuilder)
    {
        this.pdfBuilder = pdfBuilder;
    }
}
