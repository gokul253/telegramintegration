/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.deloitte.telegram.integration.service;

import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

public interface TelegramintegrationService
{
    void updateCustomersWithTelegramChatId();

    String getHybrisLogoUrl(String logoCode);

    void createLogo(String logoCode);

    void sendOrderConfirmationMessage(OrderModel order);

    void sendAbandonedCartNotification(CartModel cart);

    public void sendOrderNotificationMessage(final ConsignmentModel consignment);

    void sendGiveAwayCouponMessage(List<CouponData> giftCoupons, OrderModel order);

    void sendOrderDetailsDocument(AbstractOrderModel order);

}
